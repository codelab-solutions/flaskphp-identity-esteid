<?php


/**
 *
 *   FlaskPHP-Identity-EstEID
 *   ------------------------
 *   Digital signing provider for EstEID
 *
 * @author   Codelab Solutions OÜ <codelab@codelab.ee>
 * @author   Advanced Solutions OÜ <info@asolutions.ee>
 * @license  https://www.flaskphp.com/LICENSE MIT
 *
 */

namespace Codelab\FlaskPHP\Identity\EstEID;

use Codelab\FlaskPHP;
use Codelab\FlaskPHP\Mailer\Mailer;
use Exception;


/**
 * Class SignController
 * @package Codelab\FlaskPHP\Identity\EstEID
 */
class SignController
{

    /**
     *   Dev mode?
     * @var bool
     * @access public
     */

    public $devMode = false;


    /**
     *   Service language
     * @var string
     * @access public
     */

    public $serviceLanguage = 'EST';

    /**
     *   JAVA class path
     * @var string
     * @access public
     */

    private $javaClassPath = '.:./activation-1.1.1.jar:' .
    './annotations-java5-22.0.0.jar:' .
    './aopalliance-repackaged-2.6.1.jar:' .
    './bcpkix-jdk15on-1.70.jar:' .
    './bcprov-jdk15on-1.70.jar:' .
    './bcprov-jdk15on.jar:' .
    './bcutil-jdk15on-1.70.jar:' .
    './commons-cli-1.5.0.jar:' .
    './commons-codec-1.15.jar:' .
    './commons-collections4-4.4.jar:' .
    './commons-io-2.11.0.jar:' .
    './commons-lang-2.6.jar:' .
    './commons-lang3-3.12.0.jar:' .
    './commons-logging-1.2.jar:' .
    './commons-math3-3.6.1.jar:' .
    './contiperf-2.4.3.jar:' .
    './ddoc4j-5.1.0.jar:' .
    './digidoc4j-5.1.0.jar:' .
    './digidoc4j-util.jar:' .
    './dss-alert-5.11.1.d4j.1.jar:' .
    './dss-asic-cades-5.11.1.d4j.1.jar:' .
    './dss-asic-common-5.11.1.d4j.1.jar:' .
    './dss-asic-xades-5.11.1.d4j.1.jar:' .
    './dss-cades-5.11.1.d4j.1.jar:' .
    './dss-common-validation-jaxb-5.4.d4j.1.jar:' .
    './dss-crl-parser-5.11.1.d4j.1.jar:' .
    './dss-crl-parser-stream-5.11.1.d4j.1.jar:' .
    './dss-crl-parser-x509crl-5.11.1.d4j.1.jar:' .
    './dss-detailed-report-jaxb-5.11.1.d4j.1.jar:' .
    './dss-diagnostic-jaxb-5.11.1.d4j.1.jar:' .
    './dss-document-5.11.1.d4j.1.jar:' .
    './dss-enumerations-5.11.1.d4j.1.jar:' .
    './dss-i18n-5.11.1.d4j.1.jar:' .
    './dss-jaxb-common-5.11.1.d4j.1.jar:' .
    './dss-jaxb-parsers-5.11.1.d4j.1.jar:' .
    './dss-model-5.11.1.d4j.1.jar:' .
    './dss-pades-5.11.1.d4j.1.jar:' .
    './dss-pades-pdfbox-5.11.1.d4j.1.jar:' .
    './dss-policy-jaxb-5.11.1.d4j.1.jar:' .
    './dss-reports-5.4.d4j.1.jar:' .
    './dss-service-5.11.1.d4j.1.jar:' .
    './dss-simple-certificate-report-jaxb-5.11.1.d4j.1.jar:' .
    './dss-simple-report-jaxb-5.11.1.d4j.1.jar:' .
    './dss-spi-5.11.1.d4j.1.jar:' .
    './dss-token-5.11.1.d4j.1.jar:' .
    './dss-tsl-jaxb-5.4.d4j.1.jar:' .
    './dss-tsl-validation-5.11.1.d4j.1.jar:' .
    './dss-utils-5.11.1.d4j.1.jar:' .
    './dss-utils-apache-commons-5.11.1.d4j.1.jar:' .
    './dss-xades-5.11.1.d4j.1.jar:' .
    './fontbox-2.0.26.jar:' .
    './hamcrest-core-2.1.jar:' .
    './hamcrest-library-2.1.jar:' .
    './hk2-api-2.6.1.jar:' .
    './hk2-locator-2.6.1.jar:' .
    './hk2-utils-2.6.1.jar:' .
    './httpclient5-5.2.1.jar:' .
    './httpcore5-5.2.jar:' .
    './httpcore5-h2-5.2.jar:' .
    './istack-commons-runtime-3.0.12.jar:' .
    './jackson-annotations-2.11.2.jar:' .
    './jackson-core-2.11.2.jar:' .
    './jackson-databind-2.11.2.jar:' .
    './jackson-jaxrs-base-2.8.5.jar:' .
    './jackson-jaxrs-json-provider-2.8.5.jar:' .
    './jackson-module-jaxb-annotations-2.10.1.jar:' .
    './jakarta.activation-1.2.2.jar:' .
    './jakarta.activation-api-1.2.2.jar:' .
    './jakarta.annotation-api-1.3.5.jar:' .
    './jakarta.inject-2.6.1.jar:' .
    './jakarta.ws.rs-api-2.1.6.jar:' .
    './jakarta.xml.bind-api-2.3.3.jar:' .
    './javassist-3.25.0-GA.jar:' .
    './javax.activation-1.2.0.jar:' .
    './javax.annotation-api-1.3.2.jar:' .
    './javax.inject-1.jar:' .
    './javax.ws.rs-api-2.1.jar:' .
    './jaxb-api-2.3.0.jar:' .
    './jaxb-core-2.3.0.1.jar:' .
    './jaxb-impl-2.3.8.jar:' .
    './jaxb-runtime-2.3.8.jar:' .
    './jcabi-matchers-1.4.jar:' .
    './jersey-apache-connector-2.24.1.jar:' .
    './jersey-client-2.31.jar:' .
    './jersey-common-2.31.jar:' .
    './jersey-entity-filtering-2.31.jar:' .
    './jersey-guava-2.24.1.jar:' .
    './jersey-hk2-2.31.jar:' .
    './jersey-media-json-jackson-2.31.jar:' .
    './jersey-media-moxy-2.24.1.jar:' .
    './json-20190722.jar:' .
    './jsonassert-1.5.0.jar:' .
    './junit-4.12.jar:' .
    './junit-toolbox-1.11.jar:' .
    './logback-classic-1.2.3.jar:' .
    './logback-core-1.2.3.jar:' .
    './mid-rest-java-client-1.3.jar:' .
    './mockito-core-2.28.2.jar:' .
    './org.eclipse.persistence.antlr-2.5.3-RC1.jar:' .
    './org.eclipse.persistence.asm-2.5.3-RC1.jar:' .
    './org.eclipse.persistence.core-2.5.3-RC1.jar:' .
    './org.eclipse.persistence.moxy-2.5.3-RC1.jar:' .
    './osgi-resource-locator-1.0.3.jar:' .
    './pdfbox-2.0.26.jar:' .
    './slf4j-api-1.7.36.jar:' .
    './snakeyaml-2.0.jar:' .
    './specs-trusted-list-5.11.1.d4j.1.jar:' .
    './specs-validation-report-5.11.1.d4j.1.jar:' .
    './specs-xades-5.11.1.d4j.1.jar:' .
    './specs-xmldsig-5.11.1.d4j.1.jar:' .
    './spring-boot-1.5.8.RELEASE.jar:' .
    './spring-context-4.3.12.RELEASE.jar:' .
    './system-rules-1.19.0.jar:' .
    './txw2-2.3.8.jar:' .
    './validation-api-2.0.1.Final.jar:' .
    './validation-policy-5.11.1.d4j.1.jar:' .
    './wiremock-2.25.1.jar:' .
    './xmlsec-2.1.4.jar:' .
    './xmlsec-2.3.2.jar:' .
    './xmlunit-1.6.jar';


    /**
     *
     *   Constructor
     *   -----------
     * @access public
     * @param bool $devMode
     * @return Sign
     *
     * @throws SignException
     */

    public function __construct(bool $devMode = null)
    {
        // Add locale
        Flask()->Locale->addLocalePath(__DIR__ . '/../locale');

        if ($devMode !== null) {
            $this->devMode = $devMode;
        } else {
            $this->devMode = Flask()->Debug->devEnvironment;
        }

        // Language
        if (Flask()->Config->get('identity.esteid.language')) {
            $this->serviceLanguage = Flask()->Config->get('identity.esteid.language');
        }
    }


    /**
     *
     *   Start signing session
     *   ---------------------
     * @access public
     * @param string $container
     * @return SessionStartResponse
     *
     * @throws Exception
     */

    public function startSession(string $container): SessionStartResponse
    {
        $uniqueName = uniqid();
        $filename = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.txt';
        $digestHashFile = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.digest.txt';
        $dataToSignFile = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.datatosign.bin';
        $signerCertificateFile = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.signerCert.txt';
        $BDOCFile = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.asice';

        try {
            file_put_contents($BDOCFile, $container);
            file_put_contents($filename, '');
            file_put_contents($dataToSignFile, '');
            file_put_contents($digestHashFile, '');
            file_put_contents($signerCertificateFile, '');

            Flask()->Session->set('eid.temp.output.file', $filename);
            Flask()->Session->set('eid.temp.output.name', $uniqueName);
            Flask()->Session->set('eid.temp.output.data', $dataToSignFile);
            Flask()->Session->set('eid.temp.output.digest', $digestHashFile);
            Flask()->Session->set('eid.temp.output.signerCert', $signerCertificateFile);

        } catch (Exception $e) {
            throw new SignException('Session start failed');
        }

        $response = new SessionStartResponse();
        $response
            ->setFilename($filename)
            ->setDataToSignFile($dataToSignFile)
            ->setDigestHashFile($digestHashFile)
            ->setSignerCertificateFile($signerCertificateFile)
            ->setBdocFileName($uniqueName);

        return $response;
    }


    /**
     *
     *   Prepare signature
     *   -----------------
     * @access public
     * @param string $signerCertificate Signer's certificate
     * @throws Exception|SignException
     * @return PrepareResponse
     *
     */

    public function prepareSignature(string $signerCertificate)
    {
        $filename = Flask()->Session->get('eid.temp.output.file');
        $uniqueName = Flask()->Session->get('eid.temp.output.name');
        $dataToSignFile = Flask()->Session->get('eid.temp.output.data');
        $digestHashFile = Flask()->Session->get('eid.temp.output.digest');
        $signerCertificateFile = Flask()->Session->get('eid.temp.output.signerCert');

        file_put_contents($filename, '');
        file_put_contents($dataToSignFile, '');
        file_put_contents($digestHashFile, '');
        file_put_contents($signerCertificateFile, '');

        if (Flask()->Debug->devEnvironment) {
            $environment = 'TEST';
        } else {
            $environment = 'PROD';
        }

        file_put_contents($signerCertificateFile, $signerCertificate);

        $BDOCFile = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.asice';

        $cmd = 'java -classpath ' . $this->javaClassPath . ' ' .
            'GenerateHash '
            . $signerCertificate . ' '
            . $BDOCFile . ' '
            . $filename . ' '
            . $dataToSignFile . ' '
            . $digestHashFile . ' '
            . $environment . ' '
            . '>/dev/null 2>&1 &';
        $errors = '';
        exec_with_cwd($cmd, realpath(__DIR__ . '/../java/'), $errors);

        for ($t = 0; $t <= 10; $t++) {
            sleep(1);
            if ($file = fopen($filename, "r")) {
                while (!feof($file)) {
                    $line = fgets($file);
                    if (strpos($line, '[error]') !== false || strpos($line, 'Exception in thread "main"') !== false) {
                        $exception = trim(str_replace('[error] ', '', $line));

                        // Notify
                        $this->notifyDeveloper(
                            'Hash generation failed: ' . $exception,
                            'Java cmd: ' . $cmd . "\n\nOutput:\n" . $errors,
                            [
                                $filename,
                                $BDOCFile,
                                $dataToSignFile,
                                $digestHashFile,
                                $signerCertificateFile
                            ]
                        );

                        // Delete temp files
                        /*
                        file_exists($filename) && unlink($filename);
                        file_exists($BDOCFile) && unlink($BDOCFile);
                        file_exists($dataToSignFile) && unlink($dataToSignFile);
                        file_exists($digestHashFile) && unlink($digestHashFile);
                        file_exists($signerCertificateFile) && unlink($signerCertificateFile);
                        */

                        // Throw exception
                        throw new SignException($exception);
                    } elseif (strpos($line, '[dataToSignInHex]') !== false) {
                        $dataToSignInHex = trim(str_replace('[dataToSignInHex] ', '', $line));
                        if ($dataToSignInHex) {
                            $digest = new PrepareResponse();
                            $digest->setHex(trim($dataToSignInHex));
                            return $digest;
                        }
                    }
                }
            }
        }

        // Notify
        $this->notifyDeveloper(
            'Hash generation failed',
            'Java cmd: ' . $cmd . "\n\nOutput:\n" . $errors,
            [
                $filename,
                $BDOCFile,
                $dataToSignFile,
                $digestHashFile,
                $signerCertificateFile
            ]
        );

        // Delete temp files
        /*
        file_exists($filename) && unlink($filename);
        file_exists($BDOCFile) && unlink($BDOCFile);
        file_exists($dataToSignFile) && unlink($dataToSignFile);
        file_exists($digestHashFile) && unlink($digestHashFile);
        file_exists($signerCertificateFile) && unlink($signerCertificateFile);
        */

        // Throw exception
        throw new SignException('Hash generation failed!');
    }


    /**
     *
     *   Finalize signature
     *   ------------------
     * @access public
     * @param string $signatureHex Signer's signature
     * @throws SignException
     * @return SignResponse
     *
     */

    public function finalizeSignature(string $signatureHex)
    {
        $filename = Flask()->Session->get('eid.temp.output.file');
        $uniqueName = Flask()->Session->get('eid.temp.output.name');
        $dataToSignFile = Flask()->Session->get('eid.temp.output.data');
        $digestHashFile = Flask()->Session->get('eid.temp.output.digest');
        $signerCertificateFile = Flask()->Session->get('eid.temp.output.signerCert');

        if (Flask()->Debug->devEnvironment) {
            $environment = 'TEST';
        } else {
            $environment = 'PROD';
        }

        $signerCertificate = file_get_contents($signerCertificateFile);

        $BDOCFile = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.asice';

        $cmd = 'java -classpath ' . $this->javaClassPath . ' ' .
            'Sign '
            . $signerCertificate . ' '
            . $signatureHex . ' '
            . $BDOCFile . ' '
            . $filename . ' '
            . $dataToSignFile . ' '
            . $digestHashFile . ' '
            . $environment . ' '
            . '>/dev/null 2>&1 &';

        $errors = '';

        exec_with_cwd($cmd, realpath(__DIR__ . '/../java/'), $errors);

        for ($t = 0; $t <= 120; $t++) {
            sleep(1);

            if ($file = fopen($filename, "r")) {
                while (!feof($file)) {
                    $line = fgets($file);
                    if (strpos($line, '[error]') !== false || strpos($line, 'Exception in thread "main"') !== false) {
                        $exception = trim(str_replace('[error] ', '', $line));

                        // Notify
                        $this->notifyDeveloper(
                            'Signing failed: ' . $exception,
                            'Java cmd: ' . $cmd . "\n\nOutput:\n" . $errors,
                            [
                                $filename,
                                $BDOCFile,
                                $dataToSignFile,
                                $digestHashFile,
                                $signerCertificateFile
                            ]
                        );

                        // Delete temp file
                        file_exists($filename) && unlink($filename);
                        file_exists($dataToSignFile) && unlink($dataToSignFile);
                        file_exists($digestHashFile) && unlink($digestHashFile);
                        file_exists($signerCertificateFile) && unlink($signerCertificateFile);
                        file_exists($BDOCFile) && unlink($BDOCFile);

                        // Clear session
                        Flask()->Session->set('eid.temp.output.file', '');
                        Flask()->Session->set('eid.temp.output.name', '');
                        Flask()->Session->set('eid.temp.output.data', '');
                        Flask()->Session->set('eid.temp.output.digest', '');
                        Flask()->Session->set('eid.temp.output.signerCert', '');

                        // Throw exception
                        throw new SignException($exception);
                    } elseif (strpos($line, '[containerFile]') !== false) {
                        $bDocFile = trim(str_replace('[containerFile] ', '', $line));
                        if ($bDocFile) {

                            // Delete temp file
                            file_exists($filename) && unlink($filename);
                            file_exists($dataToSignFile) && unlink($dataToSignFile);
                            file_exists($digestHashFile) && unlink($digestHashFile);
                            file_exists($signerCertificateFile) && unlink($signerCertificateFile);

                            $response = new SignResponse();
                            $response->status = 'success';
                            $response->signedDocInfo = file_get_contents($bDocFile);

                            // Cleanup
                            file_exists($bDocFile) && unlink($bDocFile);
                            file_exists($BDOCFile) && unlink($BDOCFile);
                            Flask()->Session->set('eid.temp.output.file', '');
                            Flask()->Session->set('eid.temp.output.name', '');
                            Flask()->Session->set('eid.temp.output.data', '');
                            Flask()->Session->set('eid.temp.output.digest', '');
                            Flask()->Session->set('eid.temp.output.signerCert', '');

                            return $response;
                        }
                    }
                }
            }
        }

        // Notify
        $this->notifyDeveloper(
            'Signing failed',
            'Java cmd: ' . $cmd . "\n\nOutput:\n" . $errors,
            [
                $filename,
                $BDOCFile,
                $dataToSignFile,
                $digestHashFile,
                $signerCertificateFile
            ]
        );

        // Delete temp file
        file_exists($filename) && unlink($filename);
        file_exists($BDOCFile) && unlink($BDOCFile);
        file_exists($dataToSignFile) && unlink($dataToSignFile);
        file_exists($digestHashFile) && unlink($digestHashFile);
        file_exists($signerCertificateFile) && unlink($signerCertificateFile);

        throw new SignException('Signing failed!');
    }


    /**
     *
     *   Get signed document
     *   -------------------
     * @access public
     * @return string
     *
     * @throws SignException
     */

    public function getSignedDoc()
    {
        $uniqueName = Flask()->Session->get('eid.temp.output.name');
        $BDOCFile = Flask()->Config->getTmpPath() . '/' . $uniqueName . '.asice';

        try {
            return file_get_contents($BDOCFile);
        } catch (\SoapFault $soapFault) {
            throw new SignException('Error getting signed doc.');
        }
    }


    /**
     *
     *   Notify developer
     *   ----------------
     * @access public
     * @param string $error
     * @param array $attachments
     * @return void
     *
     */

    public function notifyDeveloper(string $error, string $info = '', array $attachments = [])
    {
        try {
            // Check
            if (!mb_strlen(Flask()->Config->get('dev.email'))) return;

            // Send
            $Mailer = new Mailer();
            $Mailer->setSubject("Signing error using EstEID: " . $error);
            $Mailer->Body = "Signing error using EstEID: " . $error . "\n\nAdditional info:\n\n" . $info;
            $Mailer->addAddress(Flask()->Config->get('dev.email'));
            foreach ($attachments as $a) {
                if (!file_exists($a)) continue;
                $p = pathinfo($a);
                $Mailer->addAttachment(file_get_contents($a), $p['filename']);
            }
            $Mailer->send();
        } catch (\Exception $e) {
        }
    }


}
