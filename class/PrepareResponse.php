<?php
/**
 *
 *   FlaskPHP-Identity-EstEID
 *   ------------------------
 *   Mobile ID signing response
 *
 * @author   Codelab Solutions OÜ <codelab@codelab.ee>
 * @author   Advanced Solutions OÜ <info@asolutions.ee>
 * @license  https://www.flaskphp.com/LICENSE MIT
 *
 */


namespace Codelab\FlaskPHP\Identity\EstEID;


class PrepareResponse
{
    /**
     * Digest Hex
     * @var string
     */

    public $hex = null;

    /**
     * @return string
     */
    public function getHex(): string
    {
        return $this->hex;
    }

    /**
     * @param string $hex
     * @return PrepareResponse
     */
    public function setHex(string $hex): PrepareResponse
    {
        $this->hex = $hex;
        return $this;
    }


}

