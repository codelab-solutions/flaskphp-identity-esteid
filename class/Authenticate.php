<?php


	/**
	 *
	 *   FlaskPHP-Identity-EstEID
	 *   ------------------------
	 *   Authentication provider for Estonian E-ID
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\EstEID;
	use Codelab\FlaskPHP\FlaskPHP;
	use Codelab\FlaskPHP\Template\Template;
	use Defuse\Crypto\Crypto;
	use Exception;
	use GuzzleHttp\Psr7\Uri;
	use phpseclib3\File\X509;
	use stdClass;
	use web_eid\web_eid_authtoken_validation_php\authtoken\WebEidAuthToken;
	use web_eid\web_eid_authtoken_validation_php\certificate\CertificateData;
	use web_eid\web_eid_authtoken_validation_php\certificate\CertificateLoader;
	use web_eid\web_eid_authtoken_validation_php\challenge\ChallengeNonceGenerator;
    use web_eid\web_eid_authtoken_validation_php\challenge\ChallengeNonceGeneratorBuilder;
	use web_eid\web_eid_authtoken_validation_php\challenge\ChallengeNonceStore;
    use web_eid\web_eid_authtoken_validation_php\exceptions\AuthTokenParseException;
    use web_eid\web_eid_authtoken_validation_php\exceptions\CertificateDecodingException;
	use web_eid\web_eid_authtoken_validation_php\exceptions\ChallengeNonceExpiredException;
	use web_eid\web_eid_authtoken_validation_php\exceptions\ChallengeNonceGenerationException;
	use web_eid\web_eid_authtoken_validation_php\exceptions\ChallengeNonceNotFoundException;
	use web_eid\web_eid_authtoken_validation_php\validator\AuthTokenValidator;
	use web_eid\web_eid_authtoken_validation_php\validator\AuthTokenValidatorBuilder;


	class Authenticate
	{


		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $forceDev Force dev environment
		 *   @return void
		 *	 @throws Exception
		 *
		 */

		public function __construct( bool $forceDev=false )
		{
			// Add locale
			Flask()->Locale->addLocalePath(__DIR__.'/../locale');

			// Init
			$this->initEstEID($forceDev);
		}


		/**
		 *
		 *   Init the provider
		 *   -----------------
		 *   @access public
		 *   @param bool $forceDev Force dev environment
		 *   @return void
		 *	 @throws Exception
		 *
		 */

		public function initEstEID( bool $forceDev=false )
		{
			// This can be extended in the subclass if necessary.
		}


		/**
		 *
		 *   Generate challenge
		 *   -----------------
		 *   @access public
		 *   @return ChallengeNonceGenerator
		 *	 @throws RuntimeException
		 *
		 */

		public function generator(): ChallengeNonceGenerator
		{
            return (new ChallengeNonceGeneratorBuilder())
                ->withNonceTtl(300) // challenge nonce TTL in seconds, default is 300 (5 minutes)
                ->build();
        }

		/**
		 *
		 *   Load certificates
		 *   -----------------
		 *   @access public
		 *   @return array
		 *	 @throws CertificateDecodingException
		 *
		 */

		public function trustedIntermediateCACertificates(): array
		{
			return CertificateLoader::loadCertificatesFromResources(
				__DIR__."/../trusted_certificates/esteid2018.pem.crt",  __DIR__."/../trusted_certificates/ESTEID-SK_2015.pem.crt"
			);
		}

		/**
		 *
		 *   Get nonce
		 *   -----------------
		 *   @access public
		 *   @return string
		 *
		 */

		public function generateChallengeNonce(): string
		{
			return $this->generator()->generateAndStoreNonce()->getBase64EncodedNonce();
		}


		/**
		 *
		 *   Validate token
		 *   -----------------
		 *   @access public
		 *   @param string $Uri
		 *   @return AuthTokenValidator
		 *   @throws CertificateDecodingException
		 *
		 */

		public function tokenValidator(string $Uri): AuthTokenValidator
		{
			return (new AuthTokenValidatorBuilder())
				->withSiteOrigin(new Uri($Uri))
				->withTrustedCertificateAuthorities(...$this->trustedIntermediateCACertificates())
				->build();
		}


		/**
		 *
		 *   Get person name from certificate
		 *   -----------------
		 *   @access public
		 *   @param X509 $userCertificate
		 * 	 @return string
		 */

		public function getPrincipalNameFromCertificate(X509 $userCertificate): string
		{
			try
			{
				return CertificateData::getSubjectGivenName($userCertificate) . " " . CertificateData::getSubjectSurname($userCertificate);
			}
			catch (Exception $e)
			{
				return CertificateData::getSubjectCN($userCertificate);
			}
		}


		/**
		 *
		 *   Validate token
		 *   -----------------
		 * @access public
		 * @param string $authToken
		 * @return stdClass
		 * @throws AuthenticateException
		 */

		public function getCertificateData(string $authToken): stdClass
		{
			try
			{
				$challengeNonce = (new ChallengeNonceStore())->getAndRemove();
				if ($challengeNonce === null) throw new ChallengeNonceExpiredException();

				// Build token validator
				$tokenValidator = $this->tokenValidator(Flask()->Config->get('app.url'));

				// Validate token
				$cert = $tokenValidator->validate(new WebEidAuthToken($authToken), $challengeNonce->getBase64EncodedNonce());

				session_regenerate_id();

				$response = new stdClass();
				$response->firstName=CertificateData::getSubjectGivenName($cert);
				$response->lastName=CertificateData::getSubjectSurname($cert);
				$response->idCode=preg_replace("/^PNOEE-/","",CertificateData::getSubjectIdCode($cert));
				$response->country=CertificateData::getSubjectCountryCode($cert);

				// Return
				return $response;
			}
			catch (CertificateDecodingException|AuthTokenParseException|ChallengeNonceExpiredException $e)
			{
				throw new AuthenticateException(Flask()->Locale->get('FLASK.IDENTITY.EstEID.Error.ErrorParsingCN').(Flask()->Debug->devEnvironment?' -- '.$e->getMessage():''));
      }
		}
	}


?>