<?php


/**
 *
 *   FlaskPHP-Identity-EstEID
 *   ------------------------
 *   Mobile ID signing response
 *
 * @author   Codelab Solutions OÜ <codelab@codelab.ee>
 * @author   Advanced Solutions OÜ <info@asolutions.ee>
 * @license  https://www.flaskphp.com/LICENSE MIT
 *
 */


namespace Codelab\FlaskPHP\Identity\EstEID;


/**
 * Class SessionStartResponse
 * @package Codelab\FlaskPHP\Identity\EstEID
 */
class SessionStartResponse
{

    /**
     * @var string
     */
    public $filename;
    /**
     * @var string
     */
    public $bdocfilename;
    /**
     * @var string
     */
    public $dataToSignFile;
    /**
     * @var string
     */
    public $digestHashFile;
    /**
     * @var string
     */
    public $signerCertificateFile;

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return SessionStartResponse
     */
    public function setFilename(string $filename): SessionStartResponse
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     */
    public function getBdocfilename(): string
    {
        return $this->bdocfilename;
    }

    /**
     * @param string $bdocfilename
     * @return SessionStartResponse
     */
    public function setBdocfilename(string $bdocfilename): SessionStartResponse
    {
        $this->bdocfilename = $bdocfilename;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataToSignFile(): string
    {
        return $this->dataToSignFile;
    }

    /**
     * @param string $dataToSignFile
     * @return SessionStartResponse
     */
    public function setDataToSignFile(string $dataToSignFile): SessionStartResponse
    {
        $this->dataToSignFile = $dataToSignFile;
        return $this;
    }

    /**
     * @return string
     */
    public function getDigestHashFile(): string
    {
        return $this->digestHashFile;
    }

    /**
     * @param string $digestHashFile
     * @return SessionStartResponse
     */
    public function setDigestHashFile(string $digestHashFile): SessionStartResponse
    {
        $this->digestHashFile = $digestHashFile;
        return $this;
    }

    /**
     * @return string
     */
    public function getSignerCertificateFile(): string
    {
        return $this->signerCertificateFile;
    }

    /**
     * @param string $signerCertificateFile
     * @return SessionStartResponse
     */
    public function setSignerCertificateFile(string $signerCertificateFile): SessionStartResponse
    {
        $this->signerCertificateFile = $signerCertificateFile;
        return $this;
    }


}
