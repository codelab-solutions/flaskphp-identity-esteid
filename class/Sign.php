<?php


	/**
	 *
	 *   FlaskPHP-Identity-EstEID
	 *   ------------------------
	 *   Digital signing provider for Mobile ID
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\EstEID;


	use Codelab\FlaskPHP;


	class Sign
	{


		/**
		 *   Dev mode?
		 *   @var bool
		 *   @access public
		 */

		public $devMode = false;


		/**
		 *   Service language
		 *   @var string
		 *   @access public
		 */

		public $serviceLanguage = 'EST';



		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param bool $devMode
		 *   @throws SignException
		 *   @return Sign
		 *
		 */

		public function __construct( bool $devMode=null )
		{
			// Add locale
			Flask()->Locale->addLocalePath(__DIR__.'/../locale');

			// Init
			$this->initEstEID($devMode);
		}


		/**
		 *
		 *   Init the provider
		 *   -----------------
		 *   @access public
		 *   @param bool $devMode Force dev environment
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function initEstEID( bool $devMode=null )
		{
			// Dev mode
			if ($devMode!==null)
			{
				$this->devMode=$devMode;
			}
			else
			{
				$this->devMode=Flask()->Debug->devEnvironment;
			}

			// Language
			if (Flask()->Config->get('identity.esteid.language'))
			{
				$this->serviceLanguage=Flask()->Config->get('identity.esteid.language');
			}
		}


		/**
		 *
		 *   Init SOAP client
		 *   ----------------
		 *   @access private
		 *   @return \SoapClient
		 *
		 */

		private function initSoapClient()
		{
			// SOAP options
			$streamOptions=array(
				'http' => array(
					'user_agent' => 'PHPSoapClient'
				)
			);
			$streamContext=stream_context_create($streamOptions);
			$soapOptions = array(
				'cache_wsdl' => WSDL_CACHE_MEMORY,
				'stream_context' => $streamContext,
				'trace' => true,
				'encoding' => 'utf-8'
			);

			// Init SOAP client
			if ($this->devMode)
			{
				$WSDL='https://tsp.demo.sk.ee/dds.wsdl';
				$this->serviceName='Testimine';
			}
			else
			{
				$WSDL='https://digidocservice.sk.ee/?wsdl';
			}

			$soapClient=new \SoapClient($WSDL, $soapOptions);
			return $soapClient;
		}


		/**
		 *
		 *   Start signing session
		 *   ---------------------
		 *   @access public
		 *   @param string $digiDocFile Digidoc file (hashcoded)
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function startSession( string $digiDocFile )
		{
			try
			{
				// Check && base64-encode file
				if (!mb_strlen($digiDocFile)) throw new SignException('No file.');
				$digiDocFile=base64_encode($digiDocFile);

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->StartSession(
					'',
					$digiDocFile,
					true
				);

				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." StartSession: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK' && intval($soapResponse['Sesscode']))
				{
					// Save Mobiil-ID data to session
					Flask()->Session->set('identity.esteid.sign.sesscode',intval($soapResponse['Sesscode']));

					// Return response
					$response=new SignResponse();
					$response->status='OK';
					$response->sessCode=intval($soapResponse['Sesscode']);
					$response->signedDocInfo=$soapResponse['SignedDocInfo'];
					return $response;
				}

				// Fail
				else
				{
					if (Flask()->Debug->debugOn)
					{
						file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug.bdoc',base64_decode($digiDocFile));
					}
					throw new SignException('Error talking to the EID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." StartSession SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.bdoc',base64_decode($digiDocFile));
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the EID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Prepare signature
		 *   -----------------
		 *   @access public
		 *   @param string $signerCertificate Signer's certificate
		 *   @param string $signerToken Signer's certificate token
		 *   @param array $signerInfo Additional signature info
		 * 	 @param int $sessionCode Session code
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function prepareSignature( string $signerCertificate, string $signerToken, array $signerInfo=null, int $sessionCode=null )
		{
			try
			{
				// Check session
				$sessCode=oneof(Flask()->Session->get('identity.esteid.sign.sesscode'), $sessionCode);
				if (!intval($sessCode)) throw new SignException('SK session code not found. Session not initialized?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Additional signer info
				$signerInfoRole=(($signerInfo!==null && array_key_exists('role',$signerInfo))?$signerInfo['role']:'');
				$signerInfoCity=(($signerInfo!==null && array_key_exists('city',$signerInfo))?$signerInfo['city']:'');
				$signerInfoState=(($signerInfo!==null && array_key_exists('state',$signerInfo))?$signerInfo['state']:'');
				$signerInfoPostalCode=(($signerInfo!==null && array_key_exists('postalcode',$signerInfo))?$signerInfo['postalcode']:'');
				$signerInfoCountry=(($signerInfo!==null && array_key_exists('country',$signerInfo))?$signerInfo['country']:'');

				// Make request
				$soapResponse=$soapClient->PrepareSignature(
					$sessCode,
					$signerCertificate,
					$signerToken,
					$signerInfoRole,
					$signerInfoCity,
					$signerInfoState,
					$signerInfoPostalCode,
					$signerInfoCountry,
					'LT_TM'
				);

				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." PrepareSignature: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK')
				{
					// Return response
					$response=new SignResponse();
					$response->status='OK';
					$response->signatureID=$soapResponse['SignatureId'];
					$response->signedInfoDigest=$soapResponse['SignedInfoDigest'];
					$response->hashType='SHA-256';
					return $response;
				}

				// Fail
				else
				{
					if (!empty($soapResponse['Status']))
					{
						throw new SignException('[[ FLASK.COMMON.Error ]]: '.$soapResponse['Status']);
					}
					else
					{
						throw new SignException('Error talking to the EID service');
					}
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." MobileSign SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the EID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Finalize signature
		 *   ------------------
		 *   @access public
		 *   @param string $signerSignatureID Signer's signature ID
		 *   @param string $signerSignature Signer's signature
		 * 	 @param int $sessionCode Session code
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function finalizeSignature( string $signerSignatureID, string $signerSignature, int $sessionCode=null )
		{
			try
			{
				// Check session
				$sessCode=oneof(Flask()->Session->get('identity.esteid.sign.sesscode'), $sessionCode);
				if (!intval($sessCode)) throw new SignException('SK session code not found. Session not initialized?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->FinalizeSignature(
					$sessCode,
					$signerSignatureID,
					$signerSignature
				);

				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." FinalizeSignature: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK')
				{
					// Return response
					$response=new SignResponse();
					$response->status='OK';
					$response->signedDocInfo=$soapResponse['SignedDocInfo'];
					return $response;
				}

				// Fail
				else
				{
					if (!empty($soapResponse['Status']))
					{
						throw new SignException('[[ FLASK.COMMON.Error ]]: '.$soapResponse['Status']);
					}
					else
					{
						throw new SignException('Error talking to the EID service');
					}
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." MobileSign SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the EID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Get signed document
		 *   -------------------
		 *   @access public
		 * 	 @param int $sessionCode Session code
		 *   @throws SignException
		 *   @return string
		 *
		 */

		public function getSignedDoc( int $sessionCode=null )
		{
			try
			{
				// Check session
				$sessCode=oneof(Flask()->Session->get('identity.esteid.sign.sesscode'), $sessionCode);
				if (!intval($sessCode)) throw new SignException('SK session code not found. Session not initialized or already closed?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->GetSignedDoc(
					$sessCode
				);

				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." GetSignedDoc: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse['Status']=='OK')
				{
					return base64_decode($soapResponse['SignedDocData']);
				}

				// Fail
				else
				{
					throw new SignException('Error talking to the EID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." GetSignedDoc SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the EID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


		/**
		 *
		 *   Close signing session
		 *   @access public
		 * 	 @param int $sessionCode Session code
		 *   @throws SignException
		 *   @return SignResponse
		 *
		 */

		public function closeSession( int $sessionCode=null )
		{
			try
			{
				// Check session
				$sessCode=oneof(Flask()->Session->get('identity.esteid.sign.sesscode'), $sessionCode);
				if (!intval($sessCode)) throw new SignException('SK session code not found. Session not initialized or already closed?');

				// Init SOAP client
				$soapClient=$this->initSoapClient();

				// Make request
				$soapResponse=$soapClient->CloseSession(
					$sessCode
				);

				// Debug
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." CloseSession: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}

				// Success
				if ($soapResponse=='OK')
				{
					// Save Mobiil-ID data to session
					Flask()->Session->set('identity.esteid.sign.sesscode',null);

					// Return response
					$response=new SignResponse();
					$response->status='OK';
					return $response;
				}

				// Fail
				else
				{
					throw new SignException('Error talking to the EID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapResponse):''));
				}
			}
			catch (\SoapFault $soapFault)
			{
				if (Flask()->Debug->debugOn)
				{
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',date('Y-m-d H:i:s')." CloseSession SoapFault: \n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',var_dump_str($soapFault)."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastRequest()."\n\n",FILE_APPEND);
					file_put_contents(Flask()->Config->getTmpPath().'/flaskphp-identity-esteid-sign.debug',$soapClient->__getLastResponse()."\n\n",FILE_APPEND);
				}
				if (!empty($soapFault->detail->message))
				{
					throw new SignException('[[ FLASK.COMMON.Error ]]: '.strval($soapFault->detail->message));
				}
				else
				{
					throw new SignException('Error talking to the EID service'.(Flask()->Debug->devEnvironment?': '.var_dump_str($soapFault):''));
				}
			}
		}


	}


?>