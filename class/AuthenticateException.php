<?php


	/**
	 *
	 *   FlaskPHP-Identity-EstEid
	 *   --------------------------
	 *   Authentication exception
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\Identity\EstEID;
	use Codelab\FlaskPHP;


	class AuthenticateException extends FlaskPHP\Exception\Exception
	{
	}


?>
